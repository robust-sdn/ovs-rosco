#include <config.h>
#include <arpa/inet.h>

#include <ofproto/connmgr.h>
#include <openvswitch/list.h>
#include <openvswitch/ofp-msgs.h>
#include <openvswitch/ofp-print.h>
#include <openflow/openflow-common.h>
#include <openvswitch/ofpbuf.h>
#include <openvswitch/vlog.h>

#include "connmgr_bm.h"
#include "ofproto_bls_set.h"
#include "ofproto_rep_set.h"
#include "rosco_utils.h"
#include "ofproto_handle_rosco.h"

VLOG_DEFINE_THIS_MODULE(ofproto_handle_rosco);

#define IS_BLS_MSG(MSG) \
    ((MSG->type) == 0xAA)

#define IS_BLS_AGG_MSG(MSG) \
    ((MSG->type) == 0xAB || ((MSG->type) == 0xAD))

#define IS_BLS_AGG_LDR_MSG(MSG) \
    ((MSG->type) == 0xAD)

#define IS_REP_MSG(MSG) \
    (ntohl(MSG->xid) >= 0x80000000)

bool ofproto_bls_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
               void (*handle_openflow)(struct ofconn *,  const struct ofpbuf *ofp_msg));

bool ofproto_bls_agg_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
               void (*handle_openflow)(struct ofconn *,  const struct ofpbuf *ofp_msg));

bool ofproto_rep_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
               void (*handle_openflow)(struct ofconn *,  const struct ofpbuf *ofp_msg));

bool
ofproto_handle_rosco(struct ofconn *ofconn, struct ofpbuf* of_msg,
        void (*handle_openflow)(struct ofconn *,
            const struct ofpbuf *ofp_msg))
{
    const struct ofp_header *oh = of_msg->data;
    bool rc = false;

    if(IS_BLS_MSG(oh)) {
        rc = ofproto_bls_handle_ofmsg(ofconn, of_msg, handle_openflow);
    } else if(IS_BLS_AGG_MSG(oh)) {
        rc = ofproto_bls_agg_handle_ofmsg(ofconn, of_msg, handle_openflow);
    } else if(IS_REP_MSG(oh)) {
        rc = ofproto_rep_handle_ofmsg(ofconn, of_msg, handle_openflow);
    }
    
    /* ECHO message to controllers for ledger */
    if(rc) {
        LIST_FOR_EACH(ofconn_wrap, node, connmgr_get_ofconns(mgr)) {
            struct ofconn* ofconn_itr = (struct ofconn*)ofconn_wrap;
            rosco_send_echo(ofconn_itr, of_msg);
        }
    }
    
    return false;
}

/* Wrapper for ofconn to allow access to 'private' node pointer */
struct ofconn_wrapper {
    struct ovs_list node;
};

bool
ofproto_rosco_handle_role_req(struct connmgr* mgr, struct ofconn* pri_con, const struct ofp_header* handle_oh)
{
    VLOG_INFO("SETTING PRIMARY: %p", pri_con);
    ofconn_set_role(pri_con, OFPCR12_ROLE_MASTER);
    struct ofconn_wrapper *ofconn_wrap = NULL;
    LIST_FOR_EACH(ofconn_wrap, node, connmgr_get_ofconns(mgr)) {
        struct ofconn* ofconn_itr = (struct ofconn*)ofconn_wrap;
        if(ofconn_itr != pri_con) {
            VLOG_INFO("SETTING SLAVE: %p %p", pri_con, ofconn_itr);
            ofconn_set_role(ofconn_itr, OFPCR12_ROLE_SLAVE);
        }
        rosco_send_ack(ofconn_itr, handle_oh);
    }
    return true;
}

bool
ofproto_bls_agg_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
        void (*handle_openflow)(struct ofconn *,
            const struct ofpbuf *ofp_msg))
{
    struct connmgr* mgr = ofconn_get_connmgr(ofconn);
    struct ofproto_bls_set* ofp_bls_set = connmgr_get_bls_set(mgr);
    const struct ofp_header *oh = of_msg->data;

    // VLOG_INFO("NEW AGG MSG for XID: %08X", ntohl(oh->xid));
    ofproto_bls_set_lock(ofp_bls_set);

    struct ofpbuf msg_to_handle;
    int handle_result = ofproto_bls_set_handle_agg_msg(ofp_bls_set, of_msg, &msg_to_handle);

    if(handle_result == 0) {
        rosco_send_ack(ofconn, oh);
    } else if(handle_result == 1) {
        const struct ofp_header* handle_oh = msg_to_handle.data;
        uint32_t xid = ntohl(handle_oh->xid);
        if(IS_BLS_AGG_LDR_MSG(oh)) {
            ofproto_rosco_handle_role_req(mgr, ofconn, handle_oh);    
        } else {
            struct ofconn_wrapper *ofconn_wrap = NULL;
            bool handled = false;
            LIST_FOR_EACH(ofconn_wrap, node, connmgr_get_ofconns(mgr)) {
                struct ofconn* ofconn_itr = (struct ofconn*)ofconn_wrap;
                if(ofconn_get_type(ofconn_itr) == OFCONN_PRIMARY &&
                    ofconn_get_role(ofconn_itr) != OFPCR12_ROLE_SLAVE && 
                    !handled) {
                    handle_openflow(ofconn_itr, &msg_to_handle);
                    handled = true;
                }
                rosco_send_ack(ofconn_itr, handle_oh);
            }
        }
        // VLOG_INFO("MSGOUT : %d %lld", xid & 0x7FFFFFFF, current_time_nsec());
        connmgr_benchmark_handle_reply(&msg_to_handle);
    }
    ofproto_bls_set_unlock(ofp_bls_set);
    return true;
}

bool
ofproto_bls_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
        void (*handle_openflow)(struct ofconn *,
            const struct ofpbuf *ofp_msg))
{
    struct connmgr* mgr = ofconn_get_connmgr(ofconn);
    struct ofproto_bls_set* ofp_bls_set = connmgr_get_bls_set(mgr);
    const struct ofp_header *oh = of_msg->data;

    /* Ensure quorum before processing messages */
    // VLOG_INFO("%s", ofp_to_string(of_msg->data, sizeof(struct ofp_header), NULL, NULL, 5));
    ofproto_bls_set_lock(ofp_bls_set);

       /* Add message, if it has already been retired, send acknowledgement */
    if(ofproto_bls_set_add(ofp_bls_set, of_msg, ofconn) == NULL) {
        rosco_send_ack(ofconn, oh);
    }
    //VLOG_INFO("%s", ofp_to_string(of_msg->data, sizeof(struct ofp_header), 5));

    struct ofpbuf* quorum_msg = ofproto_bls_set_get_next_quorum(ofp_bls_set);

    while(quorum_msg != NULL) {
        const struct ofp_header* quorum_oh = quorum_msg->data;
        // uint32_t quorum_xid = ntohl(quorum_oh->xid);
        //VLOG_INFO("QUORUM for XID: %08X", quorum_xid);

        struct ofconn_wrapper *ofconn_wrap = NULL;
        bool handled = false;
        LIST_FOR_EACH(ofconn_wrap, node, connmgr_get_ofconns(mgr)) {
            struct ofconn* ofconn_itr = (struct ofconn*)ofconn_wrap;

            //VLOG_INFO("OFCONN %d %d : %d %d\n", ofconn_itr->type, OFCONN_PRIMARY, ofconn_itr->role, OFPCR12_ROLE_SLAVE);
            if(ofconn_get_type(ofconn_itr) == OFCONN_PRIMARY &&
                ofconn_get_role(ofconn_itr) != OFPCR12_ROLE_SLAVE && 
                !handled) {
                handle_openflow(ofconn_itr, quorum_msg);
                handled = true;
            }
            rosco_send_ack(ofconn_itr, quorum_oh);
        }

        //VLOG_INFO("MSGOUT : %d %lld", quorum_xid & 0x7FFFFFFF, current_time_nsec());

        connmgr_benchmark_handle_reply(quorum_msg);
        ofproto_bls_set_delete(ofp_bls_set, quorum_msg);

        quorum_msg = ofproto_bls_set_get_next_quorum(ofp_bls_set);
    }
    ofproto_bls_set_unlock(ofp_bls_set);
    return true;
}

bool
ofproto_rep_handle_ofmsg(struct ofconn *ofconn, struct ofpbuf* of_msg,
        void (*handle_openflow)(struct ofconn *,
            const struct ofpbuf *ofp_msg))
{
    struct connmgr* mgr = ofconn_get_connmgr(ofconn);
    struct ofproto_rep_set* ofp_rep_set = connmgr_get_rep_set(mgr);
    const struct ofp_header *oh = of_msg->data;

    /* Ensure quorum before processing messages */
    //VLOG_INFO("%s", ofp_to_string(of_msg->data, sizeof(struct ofp_header), 5));
    ofproto_rep_set_lock(ofp_rep_set);

    /* Add message, if it has already been retired, send acknowledgement */
    if(ofproto_rep_set_add(ofp_rep_set, of_msg, ofconn) == NULL) {
        rosco_send_ack(ofconn, oh);
    }
    //VLOG_INFO("%s", ofp_to_string(of_msg->data, sizeof(struct ofp_header), 5));
        
    struct ofpbuf *quorum_msg = ofproto_rep_set_get_next_quorum(ofp_rep_set);

    while(quorum_msg != NULL) {
        const struct ofp_header *quorum_oh = quorum_msg->data;
         //uint32_t quorum_xid = ntohl(quorum_msg->xid);
        //VLOG_INFO("QUORUM for XID: %08X", quorum_xid);

        struct ofconn_wrapper *ofconn_wrap = NULL;
        bool handled = false;
        LIST_FOR_EACH(ofconn_wrap, node, connmgr_get_ofconns(mgr)) {
            struct ofconn* ofconn_itr = (struct ofconn*)ofconn_wrap;
                
            //VLOG_INFO("OFCONN %d %d : %d %d\n", ofconn_itr->type, OFCONN_PRIMARY, ofconn_itr->role, OFPCR12_ROLE_SLAVE);
            if(ofconn_get_type(ofconn_itr) == OFCONN_PRIMARY &&
                ofconn_get_role(ofconn_itr) == OFPCR12_ROLE_SLAVE && 
                !handled) {
                handle_openflow(ofconn_itr, quorum_msg);
                handled = true;
            }
            rosco_send_ack(ofconn_itr, quorum_oh);
        }

        //VLOG_INFO("MSGOUT : %s %d %lld", mgr->name, quorum_xid & 0x7FFFFFFF, rosco_time_nsec());

        connmgr_benchmark_handle_reply(quorum_msg);
        ofproto_rep_set_delete(ofp_rep_set, quorum_msg);

        quorum_msg = ofproto_rep_set_get_next_quorum(ofp_rep_set);
    }
    ofproto_rep_set_unlock(ofp_rep_set);
    return true;
}
