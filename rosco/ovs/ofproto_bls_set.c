#include <config.h>
#include <stdint.h>

#include <timeval.h>
#include <openvswitch/hmap.h>
#include <openvswitch/ofpbuf.h>
#include <openvswitch/list.h>
#include <openvswitch/vlog.h>
#include <openflow/openflow-common.h>
#include <openvswitch/rconn.h>
#include <ofproto/connmgr.h>

#include "bls_utils.h"
#include "ofproto_bls_set.h"
#include "rosco_utils.h"

VLOG_DEFINE_THIS_MODULE(ofproto_bls_set);

struct ofproto_bls_set_node {
    struct hmap_node hmap_node;
    struct ofpbuf *ofp_buf;
    void* msg_signature;
    struct hmap conn_map;
    struct ovs_list signatures;
    uint32_t count;
    long long int start_time;
};

void ofproto_bls_set_init_default(struct ofproto_bls_set* set)
{
    // TODO: change this so that these are NOT hard coded
    return ofproto_bls_set_init(set,
        "/usr/local/share/pbc/system.param",
        "/usr/local/share/pbc/pairing.param",
        "/usr/local/share/pbc/bls_pk_file");
}

void ofproto_bls_set_init(struct ofproto_bls_set* set, const char* sys_param_file, const char* pairing_param_file, const char* pk_file)
{
    VLOG_INFO("Initializing BLS Set: <%s> <%s> <%s>", sys_param_file, pairing_param_file, pk_file);
    hmap_init(&(set->map));
    hmap_init(&(set->retired_xids));
    set->cfg = bls_config_init(sys_param_file, pairing_param_file, pk_file);
    if (set->cfg == NULL) {
        VLOG_ERR("Failed to initialize BLS config");
        ovs_abort(1, "Failed to initialize BLS config");
    }
    set->min_xid = 0;
    set->min_quorum = 2 * bls_config_getT(set->cfg) + 1;
    ovs_mutex_init(&(set->mutex));
}

void ofproto_bls_set_lock(struct ofproto_bls_set* set)
{
    ovs_mutex_lock(&(set->mutex));
}

void ofproto_bls_set_unlock(struct ofproto_bls_set* set)
{
    ovs_mutex_unlock(&(set->mutex));
}

/*
static inline uint32_t
ofproto_bls_set_get_count(const struct ofproto_bls_set_node *node)
{
    if(node) {
        return node->count;
    }
    return 0;
}

static inline uint32_t 
ofproto_bls_set_get_xid(const struct ofproto_bls_set_node *node)
{
    return node->hmap_node.hash;
}

static inline struct ofpbuf*
ofproto_bls_set_get_ofpbuf(const struct ofproto_bls_set_node *node)
{
    if(node) {
        return node->ofp_buf;
    }
    return NULL;
}
*/

static inline struct ofproto_bls_set_node*
ofproto_bls_set_node_new__(const struct ofpbuf *buffer)
{
    struct ofproto_bls_set_node *new_node = (struct ofproto_bls_set_node*)xmalloc(sizeof(struct ofproto_bls_set_node));
    new_node->hmap_node = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
    new_node->ofp_buf = ofpbuf_clone(buffer);
    hmap_init(&new_node->conn_map);
    ovs_list_init(&new_node->signatures);
    new_node->count = 0;
    new_node->start_time = current_time_nsec();
    return new_node;
}

static inline void
ofproto_bls_set_node_add_ofconn__(struct ofproto_bls_set_node *node, const struct ofconn *conn)
{
    if(hmap_first_with_hash(&node->conn_map, (size_t)conn) == NULL) {
        struct hmap_node *conn_node = (struct hmap_node*)xmalloc(sizeof(struct hmap_node));
        (*conn_node) = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
        hmap_insert(&node->conn_map, conn_node, (size_t)conn);
    }
}

static inline void
ofproto_bls_set_node_add_signature__(struct ofproto_bls_set_node *node, int node_id, const void* signature, size_t signature_length)
{
    struct bls_signature *sig_node = (struct bls_signature*)xmalloc(sizeof(struct bls_signature));
    sig_node->node_id = node_id;
    sig_node->signature = xmalloc(signature_length);
    memcpy(sig_node->signature, signature, signature_length);
    sig_node->signature_length = signature_length;
    ovs_list_push_back(&node->signatures, &sig_node->node);
}

static inline void
ofproto_bls_set_node_delete_ofconn__(struct ofproto_bls_set_node *node, struct hmap_node *conn_node)
{
    hmap_remove(&node->conn_map, conn_node);
    free(conn_node);
}

static inline void
ofproto_bls_set_node_delete_signature__(struct ofproto_bls_set_node *node, struct bls_signature *sig_node)
{
    ovs_list_remove(&sig_node->node);
    free(sig_node->signature);
    free(sig_node);
}

static inline bool 
ofproto_bls_set_node_ofconn_exists__(const struct ofproto_bls_set_node *node, const struct ofconn *conn)
{
    return (hmap_first_with_hash(&node->conn_map, (size_t)conn) != NULL);
}

static inline bool
ofproto_bls_set_node_signature_exists__(const struct ofproto_bls_set_node *node, const void* signature, size_t signature_length)
{
    struct bls_signature* sig_node;
    LIST_FOR_EACH(sig_node, node, &node->signatures) {
        if(sig_node->signature_length == signature_length) {
            if(memcmp(sig_node->signature, signature, signature_length) == 0) {
                return true;
            }
        }
    }
    return false;
}

static inline void
ofproto_bls_set_add_retired_xid__(struct ofproto_bls_set* set, uint32_t xid)
{
    if(hmap_first_with_hash(&set->retired_xids, xid) == NULL) {
        //VLOG_INFO("ADD RETIRED: %08X", xid);
        struct hmap_node *retired_xid = (struct hmap_node*)xmalloc(sizeof(struct hmap_node));
        (*retired_xid) = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
        hmap_insert(&set->retired_xids, retired_xid, xid);
    }
}

static inline bool
ofproto_bls_set_retired_xid_exists__(const struct ofproto_bls_set *set, uint32_t xid)
{
    return (hmap_first_with_hash(&set->retired_xids, (size_t)xid) != NULL);
}

static inline struct ofproto_bls_set_node *
ofproto_bls_set_find_node__(const struct ofproto_bls_set *set, uint32_t xid, const struct ofpbuf* buffer)
{
    struct ofproto_bls_set_node *node;

    HMAP_FOR_EACH_WITH_HASH (node, hmap_node, xid, &set->map) {
        if(ofpbuf_equal(buffer, node->ofp_buf)) {
            return node;
        }
    }
    return NULL;
}

/* Deletes 'node' from 'set' and frees 'node'. */
static inline void
ofproto_bls_set_delete__(struct ofproto_bls_set *set, struct ofproto_bls_set_node *node)
{
    struct hmap_node *conn_node;
    struct bls_signature *sig_node, *next_sig_node;
    uint32_t xid = hmap_node_hash(&node->hmap_node);

    /* Remove the node from the hash and free up the message buffer */
    hmap_remove(&set->map, &node->hmap_node);
    ofpbuf_delete(node->ofp_buf);

    /* Free up the nodes connection map */
    while((conn_node = hmap_first(&node->conn_map)) != NULL) {
        ofproto_bls_set_node_delete_ofconn__(node, conn_node);
    }
    hmap_destroy(&node->conn_map);

    /* Free all storedsignatures */
    LIST_FOR_EACH_SAFE (sig_node, next_sig_node, node, &node->signatures) {
        ofproto_bls_set_node_delete_signature__(node, sig_node);
    }

    /* Free the stored quorum signature */
    bls_signature_destroy(node->msg_signature);

    /* Free the ndoe */
    free(node);

    /* Add the node's xid to the map of retired xids */
    ofproto_bls_set_add_retired_xid__(set, xid);
}

/* Removes all of the messages from 'set' with the given xid. */
void ofproto_bls_set_delete(struct ofproto_bls_set* set, const struct ofpbuf* of_msg)
{
    struct ofproto_bls_set_node *node;
    const struct ofp_header *oh = of_msg->data;
    uint32_t xid = ntohl(oh->xid);

    INIT_CONTAINER(node, hmap_first_with_hash(&set->map, xid), hmap_node);

    while(node != OBJECT_CONTAINING(NULL, node, hmap_node)) {
        //VLOG_INFO("DELETE %08X", xid);
        ofproto_bls_set_delete__(set, node);
        INIT_CONTAINER(node, hmap_first_with_hash(&set->map, xid), hmap_node);
    }

    if(set->min_xid > 0) {
        set->min_xid++;
    }
}

struct ofpbuf* ofproto_bls_set_get_next_quorum(const struct ofproto_bls_set *set)
{
    struct ofproto_bls_set_node *node = NULL;
    
    //VLOG_INFO("INITIAL QUORUM CHECK %d %d", set->min_xid, set->min_quorum);

    // Find a valid node that has a quorum of responses
    INIT_CONTAINER(node, hmap_first_with_hash(&set->map, set->min_xid), hmap_node);
    if(node == NULL || (node != OBJECT_CONTAINING(NULL, node, hmap_node) && node->count < set->min_quorum)) {
        return NULL;
    }

    //VLOG_INFO("PAST INITIAL QUROUM CHECK");
    //long long int start_time = current_time_nsec();

    // Verify the signatures
    //if(bls_verify_signatures(set->cfg, node->ofp_buf->data, node->ofp_buf->size, &node->signatures) == false) {
    if(bls_verify_signatures_existing(set->cfg, node->msg_signature, &node->signatures) == false) {
        const struct ofp_header *oh = node->ofp_buf->data;
        uint32_t xid = ntohl(oh->xid);
        VLOG_INFO("BAD SIGNATURE %08X", xid);
        return NULL;
    }
    
    /*
    long long int end_time = current_time_nsec();
    long long int qtime = current_time_nsec() - node->start_time;
    long long int vtime = end_time - start_time;
    const struct ofp_header* quorum_oh = node->ofp_buf->data;
    uint32_t quorum_xid = ntohl(quorum_oh->xid);
    VLOG_INFO("QUORUM for XID: %08X QTIME: %lld VTIME: %lld", quorum_xid, qtime, vtime);
    */
    
    return node->ofp_buf;
}

struct ofpbuf* ofproto_bls_set_add(struct ofproto_bls_set *set, const struct ofpbuf* of_msg, const struct ofconn *ofconn)
{
    struct ofproto_bls_set_node *node;
    const struct ofp_header *oh = of_msg->data;
    uint32_t xid = ntohl(oh->xid);

    //VLOG_INFO("RECEIVE %08X", xid);

    /* Do not add a retired message to the set */
    if(ofproto_bls_set_retired_xid_exists__(set, xid)) {
        //VLOG_INFO("RETIRED %08X", xid);
        return NULL;
    }
        
    // Pull the signature and original message 
    struct ofpbuf b = ofpbuf_const_initializer(oh, ntohs(oh->length));
    b.header = ofpbuf_pull(&b, sizeof(struct ofp_header));
    b.msg = b.data;
        
    // Pull signature and node Id
    uint8_t node_id = *(uint8_t*)ofpbuf_pull(&b, sizeof(uint8_t));
       size_t signature_length = (size_t)*(uint8_t*)ofpbuf_pull(&b, sizeof(uint8_t));
    void* signature = ofpbuf_pull(&b, signature_length);

    //VLOG_INFO("ADD %08X %s", xid, ofconn_get_name(ofconn));
    //VLOG_INFO("NODEID: %u SIG LENGTH: %u", node_id, signature_length);

    node = ofproto_bls_set_find_node__(set, xid, &b);
    if(!node) {
        // VLOG_INFO("NEW NODE %08X", xid);
        node = ofproto_bls_set_node_new__(&b);
        node->msg_signature = bls_compute_signature(set->cfg, b.data, b.size);
        hmap_insert(&set->map, &node->hmap_node, xid);
    }

    /* Do not increment the count if the message is for the same connection */
    if(!ofproto_bls_set_node_ofconn_exists__(node, ofconn) &&
        !ofproto_bls_set_node_signature_exists__(node, signature, signature_length)) {
        ofproto_bls_set_node_add_ofconn__(node, ofconn);
        ofproto_bls_set_node_add_signature__(node, node_id, signature, signature_length);
        node->count++;
        //VLOG_INFO("NEW COUNT: %08X %d: %s", xid, node->count, ofconn_get_name(ofconn));
    }

    if(set->min_xid == 0) {
        set->min_xid = xid;
    }

    return node->ofp_buf;
}

int ofproto_bls_set_handle_agg_msg(struct ofproto_bls_set *set, const struct ofpbuf* of_msg, struct ofpbuf* output)
{
    struct ofproto_bls_set_node *node;
    const struct ofp_header *oh = of_msg->data;
    uint32_t xid = ntohl(oh->xid);
    
    /* Do not add a retired message to the set */
    if(ofproto_bls_set_retired_xid_exists__(set, xid)) {
        return 0;
    }

    // Pull the signature and original message 
    (*output) = ofpbuf_const_initializer(oh, ntohs(oh->length));
    output->header = ofpbuf_pull(output, sizeof(struct ofp_header));
    output->msg = output->data;
        
    // Pull signature
       size_t signature_length = (size_t)*(uint8_t*)ofpbuf_pull(output, sizeof(uint8_t));
    void* signature = ofpbuf_pull(output, signature_length);

    if(bls_verify_signature(set->cfg, output->data, output->size, signature, signature_length)) {
        /* Add the node's xid to the map of retired xids */
        ofproto_bls_set_add_retired_xid__(set, xid);
        return 1;
    } else {
        VLOG_INFO("BAD SIGNATURE %08X", xid);
    }
    return -1;
}
