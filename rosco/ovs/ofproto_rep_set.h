#ifndef OFP_REP_SET
#define OFP_REP_SET

#include <openvswitch/hmap.h>
#include <openvswitch/thread.h>

struct ofconn;
struct ofpbuf;

struct ofproto_rep_set {
    struct hmap map;
    struct hmap retired_xids;
    uint32_t min_xid;
    struct ovs_mutex mutex;
};

#define OFPROTO_REP_SET_NULL_INITIALIZER(OFPROTO_REP_SET) \
{ \
    HMAP_INITIALIZER(&(OFPROTO_REP_SET)->map), \
    HMAP_INITIALIZER(&(OFPROTO_REP_SET)->retired_xids) \
}

void ofproto_rep_set_init(struct ofproto_rep_set* set);
struct ofpbuf* ofproto_rep_set_add(struct ofproto_rep_set *set, const struct ofpbuf* of_msg, const struct ofconn* conn);
void ofproto_rep_set_delete(struct ofproto_rep_set *set, const struct ofpbuf* of_msg);
struct ofpbuf* ofproto_rep_set_get_next_quorum(const struct ofproto_rep_set *set);
void ofproto_rep_set_lock(struct ofproto_rep_set* set);
void ofproto_rep_set_unlock(struct ofproto_rep_set* set);

#endif
