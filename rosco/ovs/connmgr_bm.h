#ifndef CONNMGR_BM
#define CONNMGR_BM

struct ofpbuf;

void connmgr_benchmark_handle_reply(const struct ofpbuf*);
void connmgr_benchmark_init(void);

#endif
