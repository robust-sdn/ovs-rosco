#include <config.h>
#include <stdint.h>

#include <timeval.h>
#include <openvswitch/hmap.h>
#include <openvswitch/ofpbuf.h>
#include <openvswitch/list.h>
#include <openvswitch/vlog.h>
#include <openflow/openflow-common.h>
#include <openvswitch/rconn.h>
#include <ofproto/connmgr.h>

#include "rosco_utils.h"
#include "ofproto_rep_set.h"

VLOG_DEFINE_THIS_MODULE(ofproto_rep_set);

#define MIN_FAILURES 1

#define QUORUM(FAILURES) \
    (FAILURES + 1)

struct ofproto_rep_set_node {
    struct hmap_node hmap_node;
    struct ofpbuf *ofp_buf;
    struct hmap conn_map;
    uint32_t count;
    long long int start_time;
};

void ofproto_rep_set_init(struct ofproto_rep_set* set)
{
    hmap_init(&(set->map));
    hmap_init(&(set->retired_xids));
    set->min_xid = 0;
    ovs_mutex_init(&(set->mutex));
}

void ofproto_rep_set_lock(struct ofproto_rep_set* set)
{
    ovs_mutex_lock(&(set->mutex));
}

void ofproto_rep_set_unlock(struct ofproto_rep_set* set)
{
    ovs_mutex_unlock(&(set->mutex));
}

static inline uint32_t
ofproto_rep_set_get_count(const struct ofproto_rep_set_node *node)
{
    if(node) {
        return node->count;
    }
    return 0;
}

static inline struct ofpbuf*
ofproto_rep_set_get_ofpbuf(const struct ofproto_rep_set_node *node)
{
    if(node) {
        return node->ofp_buf;
    }
    return NULL;
}

static inline struct ofproto_rep_set_node*
ofproto_rep_set_node_new__(const struct ofpbuf *buffer)
{
    struct ofproto_rep_set_node *new_node = (struct ofproto_rep_set_node*)xmalloc(sizeof(struct ofproto_rep_set_node));
    new_node->hmap_node = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
    new_node->ofp_buf = ofpbuf_clone(buffer);
    hmap_init(&new_node->conn_map);
    new_node->count = 0;
    new_node->start_time = current_time_nsec();
    return new_node;
}

static inline void
ofproto_rep_set_node_add_ofconn__(struct ofproto_rep_set_node *node, const struct ofconn *conn)
{
    if(hmap_first_with_hash(&node->conn_map, (size_t)conn) == NULL) {
        struct hmap_node *conn_node = (struct hmap_node*)xmalloc(sizeof(struct hmap_node));
        (*conn_node) = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
        hmap_insert(&node->conn_map, conn_node, (size_t)conn);
    }
}

static inline void
ofproto_rep_set_node_delete_ofconn__(struct ofproto_rep_set_node *node, struct hmap_node *conn_node)
{
    hmap_remove(&node->conn_map, conn_node);
    free(conn_node);
}

static inline bool 
ofproto_rep_set_node_ofconn_exists__(const struct ofproto_rep_set_node *node, const struct ofconn *conn)
{
    return (hmap_first_with_hash(&node->conn_map, (size_t)conn) != NULL);
}

static inline void
ofproto_rep_set_add_retired_xid__(struct ofproto_rep_set* set, uint32_t xid)
{
    if(hmap_first_with_hash(&set->retired_xids, xid) == NULL) {
        //VLOG_INFO("ADD RETIRED: %08X", xid);
        struct hmap_node *retired_xid = (struct hmap_node*)xmalloc(sizeof(struct hmap_node));
        (*retired_xid) = (struct hmap_node)HMAP_NODE_NULL_INITIALIZER;
        hmap_insert(&set->retired_xids, retired_xid, xid);
    }
}

static inline bool
ofproto_rep_set_retired_xid_exists__(const struct ofproto_rep_set *set, uint32_t xid)
{
    return (hmap_first_with_hash(&set->retired_xids, (size_t)xid) != NULL);
}

static inline struct ofproto_rep_set_node *
ofproto_rep_set_find_node__(const struct ofproto_rep_set *set, uint32_t xid, const struct ofpbuf* buffer)
{
    struct ofproto_rep_set_node *node;

    HMAP_FOR_EACH_WITH_HASH (node, hmap_node, xid, &set->map) {
        if(ofpbuf_equal(buffer, node->ofp_buf)) {
            return node;
        }
    }
    return NULL;
}

/* Deletes 'node' from 'set' and frees 'node'. */
static inline void
ofproto_rep_set_delete__(struct ofproto_rep_set *set, struct ofproto_rep_set_node *node)
{
    struct hmap_node *conn_node;
    uint32_t xid = hmap_node_hash(&node->hmap_node);

    /* Remove the node from the hash and free up the message buffer */
    hmap_remove(&set->map, &node->hmap_node);
    ofpbuf_delete(node->ofp_buf);

    /* Free up the nodes connection map */
    while((conn_node = hmap_first(&node->conn_map)) != NULL) {
        ofproto_rep_set_node_delete_ofconn__(node, conn_node);
    }
    hmap_destroy(&node->conn_map);

    /* Free the ndoe */
    free(node);

    /* Add the node's xid to the map of retired xids */
    ofproto_rep_set_add_retired_xid__(set, xid);
}

/* Removes all of the messages from 'set' with the given xid. */
void ofproto_rep_set_delete(struct ofproto_rep_set *set, const struct ofpbuf* of_msg)
{
    struct ofproto_rep_set_node *node;
    const struct ofp_header *oh = of_msg->data;
    uint32_t xid = ntohl(oh->xid);

    INIT_CONTAINER(node, hmap_first_with_hash(&set->map, xid), hmap_node);

    while(node != OBJECT_CONTAINING(NULL, node, hmap_node)) {
        //VLOG_INFO("DELETE %08X", xid);
        ofproto_rep_set_delete__(set, node);
        INIT_CONTAINER(node, hmap_first_with_hash(&set->map, xid), hmap_node);
    }

    if(set->min_xid > 0) {
        set->min_xid++;
    }
}

struct ofpbuf* ofproto_rep_set_get_next_quorum(const struct ofproto_rep_set *set)
{
    struct ofproto_rep_set_node *node = NULL;
   
    INIT_CONTAINER(node, hmap_first_with_hash(&set->map, set->min_xid), hmap_node);
    if(node == NULL || (node != OBJECT_CONTAINING(NULL, node, hmap_node) && ofproto_rep_set_get_count(node) < QUORUM(MIN_FAILURES))) {
        return NULL;
    }

    //long long int qtime = current_time_nsec() - node->start_time;
    //const struct ofp_header* quorum_oh = node->ofp_buf->data;
    //uint32_t quorum_xid = ntohl(quorum_oh->xid);
    //VLOG_INFO("QUORUM for XID: %08X QTIME: %lld", quorum_xid, qtime);
    
    return node->ofp_buf;
}

struct ofpbuf* ofproto_rep_set_add(struct ofproto_rep_set *set, const struct ofpbuf* of_msg, const struct ofconn *ofconn)
{
    struct ofproto_rep_set_node *node;
    const struct ofp_header *oh = of_msg->data;
    uint32_t xid = ntohl(oh->xid);

    /* Do not add a retired message to the set */
    if(ofproto_rep_set_retired_xid_exists__(set, xid)) {
        return NULL;
    }

    //VLOG_INFO("ADD %08X", xid);

    node = ofproto_rep_set_find_node__(set, xid, of_msg);
    if(!node) {
        node = ofproto_rep_set_node_new__(of_msg);
        hmap_insert(&set->map, &node->hmap_node, xid);
    }

    /* Do not increment the count if the message is for the same connection */
    if(!ofproto_rep_set_node_ofconn_exists__(node, ofconn)) {
        ofproto_rep_set_node_add_ofconn__(node, ofconn);
        node->count++;
    }

    if(set->min_xid == 0) {
        set->min_xid = xid;
    }

    return node->ofp_buf;
}
