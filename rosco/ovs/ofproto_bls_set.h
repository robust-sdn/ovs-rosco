#ifndef OFP_BLS_SET
#define OFP_BLS_SET

#include <openvswitch/hmap.h>
#include <openvswitch/thread.h>

struct ofconn;
struct ofpbuf;

struct bls_config;

struct ofproto_bls_set {
    struct hmap map;
    struct hmap retired_xids;
    struct bls_config* cfg;
    uint32_t min_xid;
    int min_quorum;
    struct ovs_mutex mutex;
};

#define OFPROTO_REP_SET_NULL_INITIALIZER(OFPROTO_REP_SET) \
{ \
    HMAP_INITIALIZER(&(OFPROTO_REP_SET)->map), \
    HMAP_INITIALIZER(&(OFPROTO_REP_SET)->retired_xids) \
}

void ofproto_bls_set_init_default(struct ofproto_bls_set* set);
void ofproto_bls_set_init(struct ofproto_bls_set* set, const char* sys_param_file, const char* pairing_param_file, const char* pk_file);
struct ofpbuf* ofproto_bls_set_add(struct ofproto_bls_set *set, const struct ofpbuf* buffer, const struct ofconn* conn);
void ofproto_bls_set_delete(struct ofproto_bls_set *set, const struct ofpbuf* buffer);
struct ofpbuf* ofproto_bls_set_get_next_quorum(const struct ofproto_bls_set *set);
void ofproto_bls_set_lock(struct ofproto_bls_set* set);
void ofproto_bls_set_unlock(struct ofproto_bls_set* set);

int ofproto_bls_set_handle_agg_msg(struct ofproto_bls_set *set, const struct ofpbuf* of_msg, struct ofpbuf* output);
#endif
