#include <config.h>
#include <lib/timeval.h>
#include <lib/ovs-atomic.h>
#include <lib/unixctl.h>
#include <openvswitch/vlog.h>
#include <openvswitch/types.h>
#include <openvswitch/ofp-packet.h>
#include <openvswitch/ofp-msgs.h>
#include <ofproto/connmgr.h>

#include "connmgr_bm.h"

static bool connmgr_bm_running = false;
static unsigned int connmgr_bm_total_replies = 0;
static atomic_count connmgr_bm_reply_count = ATOMIC_COUNT_INIT(0);
static unsigned int connmgr_bm_total_messages = 0;
struct timeval connmgr_bm_start;

static atomic_count connmgr_bm_first_sent = ATOMIC_COUNT_INIT(0);
static long long int connmgr_bm_first_send = 0;

void connmgr_benchmark_handle_request(struct unixctl_conn *conn, int argc, const char *argv[], void *aux OVS_UNUSED);
void connmgr_benchmark_reset(struct unixctl_conn *conn, int argc OVS_UNUSED, const char *argv[] OVS_UNUSED, void *aux OVS_UNUSED);

VLOG_DEFINE_THIS_MODULE(connmgr_bm);

static inline long long int
connmgr_tod_nsec(void)
{
    struct timespec cur_time;
    xclock_gettime(CLOCK_REALTIME, &cur_time);
    return cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
}

static inline void*
connmgr_benchmark_send_msgs(void* inval)
{
    struct connmgr* mgr = (struct connmgr*)inval;
    unsigned int i;

    struct ofproto_async_msg am = {
        .controller_id = 0,
        .oam = OAM_PACKET_IN,
        .pin = {
            .up = {
                .base = {
                    .packet = NULL,
                    .packet_len = 0,
                    .flow_metadata.flow.in_port.ofp_port = OFPP_LOCAL,
                    .flow_metadata.wc.masks.in_port.ofp_port
                    = u16_to_ofp(UINT16_MAX),
                    .reason = OFPR_NO_MATCH,
                    .cookie = OVS_BE64_MAX,
                },
            },
            .max_len = UINT16_MAX,
        }
    };

    for(i = 0; i < connmgr_bm_total_messages; i++) {

        if(atomic_count_inc(&connmgr_bm_first_sent) == 0) {
            connmgr_bm_first_send = connmgr_tod_nsec();
        }

        //VLOG_INFO("BM: PACKET IN SEND: %s - %d", mgr->ofproto->name, i);

        connmgr_send_async_msg(mgr, &am);
    }

    return 0;
}

void
connmgr_benchmark_handle_request(struct unixctl_conn *conn, int argc,
                                 const char *argv[], void *aux OVS_UNUSED)
{
    struct ds ds = DS_EMPTY_INITIALIZER;
    int err = 0;
    int i;
    pthread_t* sending_threads = 0;

    if(connmgr_bm_running) {
        unixctl_command_reply_error(conn, "benchmark already running");
        return;
    }

    for(i = 2; i < argc; i++) {
        if(ofproto_lookup(argv[i]) == NULL) {
            ds_put_format(&ds, "ofproto %s does not exist\n", argv[i]);
            err = 1;
        }
    }

    if(!err) {
        atomic_count_init(&connmgr_bm_first_sent, 0);
        connmgr_bm_total_messages = atoi(argv[1]);
        connmgr_bm_total_replies = connmgr_bm_total_messages * (argc - 2);
        atomic_count_init(&connmgr_bm_reply_count, 0);
        connmgr_bm_running = true;
        xgettimeofday(&connmgr_bm_start);

        VLOG_INFO("PacketIn Benchmark Request %d", connmgr_bm_total_messages);

        sending_threads = xmalloc(sizeof(pthread_t) * (argc - 2));
        for(i = 2; i < argc; i++) {
            // Create a thread to send the messages
            //   ovs_thread_create dies if thread create fails
            struct ofproto *ofproto = ofproto_lookup(argv[i]);
            sending_threads[i-2] = ovs_thread_create("sender", connmgr_benchmark_send_msgs, ofproto->connmgr);
        }
        for(i = 0; i < argc - 2; i++) {
            xpthread_join(sending_threads[i], NULL);
        }
        free(sending_threads);
    }
    
    if(err) {
        unixctl_command_reply_error(conn, ds_cstr(&ds));
    } else {
        unixctl_command_reply(conn, ds_cstr(&ds));
    }

    ds_destroy(&ds);
}

void
connmgr_benchmark_reset(struct unixctl_conn *conn, int argc OVS_UNUSED,
                        const char *argv[] OVS_UNUSED, void *aux OVS_UNUSED)
{
    connmgr_bm_running = false;
    unixctl_command_reply(conn, "DONE");
}

void
connmgr_benchmark_handle_reply(const struct ofpbuf *of_msg)
{
    const struct ofp_header *oh = of_msg->data;
    struct timeval connmgr_bm_end;
    double duration;
    enum ofptype type;

    if(!connmgr_bm_running) {
        return;
    }

    enum ofperr error = ofptype_decode(&type, oh);
    if(error) {
        return;
    }

    if(type == OFPTYPE_FLOW_MOD) {
        if(atomic_count_inc(&connmgr_bm_reply_count) == 0) {
            VLOG_INFO("REPLY LAT: %lld", connmgr_tod_nsec() - connmgr_bm_first_send);
        }

        // VLOG_INFO("REPLY COUNT: %d, %08X", atomic_count_get(&connmgr_bm_reply_count), ntohl(oh->xid));
        if(atomic_count_get(&connmgr_bm_reply_count) >= connmgr_bm_total_replies) {
            xgettimeofday(&connmgr_bm_end);
            connmgr_bm_running = false;
            duration = ((1000*(double)(connmgr_bm_end.tv_sec - connmgr_bm_start.tv_sec))
                          + (.001*(connmgr_bm_end.tv_usec - connmgr_bm_start.tv_usec)));
            VLOG_INFO("PacketIn Benchmark Finished in %.1f ms (%.0f packets/s)\n",
                           duration, connmgr_bm_total_replies / (duration / 1000.0));
        }
        //VLOG_INFO("FLOWMOD: %s", ofp_to_string(oh, ntohs(oh->length), 10));
    }
}

void
connmgr_benchmark_init(void)
{
    static bool registered;
    if (registered) {
        return;
    }
    registered = true;

    VLOG_INFO("BENCHMARK_INIT");

    unixctl_command_register("connmgr/benchmark", "count dp_name1 [dp_name2] ...", 2, UINT32_MAX,
                             connmgr_benchmark_handle_request, NULL);

    unixctl_command_register("connmgr/bmreset", "", 0, 0,
                             connmgr_benchmark_reset, NULL);
}

