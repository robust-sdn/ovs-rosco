#include <config.h>
#include <stdint.h>
#include <arpa/inet.h>

#include <timeval.h>
#include <ovs-atomic.h>
#include <openflow/openflow-common.h>
#include <openvswitch/vlog.h>
#include <openvswitch/ofp-msgs.h>
#include <openvswitch/ofpbuf.h>
#include <ofproto/connmgr.h>

#include "rosco_utils.h"

#define OFPT10_BARRIER_REPLY 19
#define OFPT11_BARRIER_REPLY 21

#define OFPT10_VENDOR       4
#define OFPT11_EXPERIMENTER 4

VLOG_DEFINE_THIS_MODULE(rosco_utils);

long long int time_diff(struct timespec* start)
{
    struct timespec cur_time;
    xclock_gettime(CLOCK_REALTIME, &cur_time);
    
    long long int cur_time_ns = cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
    long long int start_time_ns = start->tv_sec * 1000000000LL + start->tv_nsec;
    
    return (cur_time_ns - start_time_ns);
}

long long int current_time_nsec(void)
{
    struct timespec cur_time;
    xclock_gettime(CLOCK_REALTIME, &cur_time);
    return cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
}

static atomic_count rosco_xid = ATOMIC_COUNT_INIT(0);
ovs_be32 rosco_allocate_next_xid(void)
{
    return htonl(atomic_count_inc(&rosco_xid));
}

static inline enum ofpraw rosco_ack_raw(const struct ofp_header* oh)
{
    return (oh->version == OFP10_VERSION ? OFPRAW_OFPT10_BARRIER_REPLY : OFPRAW_OFPT11_BARRIER_REPLY);
}

static inline enum ofptype rosco_ack_type(const struct ofp_header* oh)
{
    return (oh->version == OFP10_VERSION ? OFPT10_BARRIER_REPLY : OFPT11_BARRIER_REPLY);
}

static inline enum ofptype rosco_echo_raw(const struct ofp_header* oh)
{
    return (oh->version == OFP10_VERSION ? OFPT10_VENDOR : OFPT11_EXPERIMENTER);
}

void rosco_allocate_set_xid(struct ofpbuf* msg)
{
    struct ofp_header *oh = msg->data;
    if(oh->type != rosco_ack_type(oh)) {
        oh->xid = rosco_allocate_next_xid();;
        // VLOG_INFO("SETTING XID 1: %02X %02X %02X %02X\n", oh->version, oh->type, oh->xid, rosco_ack_type(oh));
    }
}

void rosco_set_xid(struct ofpbuf* msg, ovs_be32 xid)
{
    struct ofp_header *oh = msg->data;
    if(oh->type != rosco_ack_type(oh)) {
        oh->xid = xid;
        // VLOG_INFO("SETTING XID 2: %02X %02X %02X\n", oh->version, oh->type, oh->xid);
    }
}

void rosco_send_ack(struct ofconn *ofconn, const struct ofp_header* oh)
{
    struct ofpbuf *buf;
    //struct ofproto *ofproto = ofconn_get_ofproto(ofconn);

    buf = ofpraw_alloc_reply(rosco_ack_raw(oh), oh, 0);
    
    //VLOG_INFO("SENDING CMD ACK %s %08X %08X %02X", ofproto->name, (unsigned int)ofconn, ntohl(oh->xid), ((struct ofp_header *)buf->data)->type);
    ofconn_send_reply(ofconn, buf);
}

void rosco_send_echo(struct ofconn *ofconn, struct ofpbuf* of_msg)
{
    const struct ofp_header *oh = of_msg->data;
    
    oh->type = rosco_echo_raw(oh);
    
    //VLOG_INFO("SENDING ECHO %s %08X %08X %02X", ofproto->name, (unsigned int)ofconn, ntohl(oh->xid), ((struct ofp_header *)buf->data)->type);
    ofconn_send_reply(ofconn, of_msg);
}
