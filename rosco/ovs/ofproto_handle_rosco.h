#ifndef OFP_HANDLE_ROSCO
#define OFP_HANDLE_ROSCO

#include <openvswitch/types.h>

struct ofconn;
struct ofpuf;

bool ofproto_handle_rosco(struct ofconn *ofconn, struct ofpbuf* of_msg,
    void (*handle_openflow)(struct ofconn *, const struct ofpbuf *ofp_msg));

#endif
