#ifndef ROSCO_UTILS
#define ROSCO_UTILS

#include <openvswitch/types.h>

struct timespec;
struct ofp_header;
struct ofpbuf;
struct ofconn;

long long int time_diff(struct timespec* start);
long long int current_time_nsec(void);
ovs_be32 rosco_allocate_next_xid(void);
void rosco_allocate_set_xid(struct ofpbuf* msg);
void rosco_set_xid(struct ofpbuf* msg, ovs_be32 xid);
void rosco_send_ack(struct ofconn *ofconn, const struct ofp_header* oh);
void rosco_send_echo(struct ofconn *ofconn, struct ofpbuf* of_msg);

#endif
