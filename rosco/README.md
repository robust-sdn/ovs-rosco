# lib-ovs-bls
 Boneh–Lynn–Shacham (BLS) Helper Library for Open vSwitch

This library requires gmp and pbc.  PBC can be downloaded from here: https://crypto.stanford.edu/pbc/ and gmp can be installed using apt-get (package libgmp-dev)

To build and install libBLS.so (a C++ library with C bindings used by the BLS OVS implementation)
The OVS bls library requries libgcrypt11-dev install with:
sudo apt-get update
sudo apt-get install libgcrypt11-dev
To build libBLS.so from source run:

1. make
1. sudo make install

The BLS library implementation includes 3 sample files to be installed for the PBC library.  These file are as follows:
* /usr/local/share/pbc/system.param - the PBC system parameters file
* /usr/local/share/pbc/pairing.param - the PBC pairing parameters file
* /usr/local/share/pbc/bls_pk_file - The BLS group public key file

These sample files are installed as part of the libBLS.so install

To build ovs run:
DEB_BUILD_OPTIONS='nocheck' fakeroot debian/rules binary
