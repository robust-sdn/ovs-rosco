#ifndef BLS_UTILS_H
#define BLS_UTILS_H

#include <openvswitch/list.h>

#ifdef __cplusplus
extern "C" {
#endif
    struct bls_signature {
        struct ovs_list node;
        void* signature;
        size_t signature_length;
        int node_id;
    };

    struct bls_config;

    int bls_config_getT(const struct bls_config* cfg);
    struct bls_config* bls_config_init(const char* system_param_path, const char* pairing_param_path, const char* public_key_path);
    void bls_config_destroy(struct bls_config* cfg);

    void* bls_compute_signature(const struct bls_config* cfg, const void* msg, size_t msg_len);
    void bls_signature_destroy(void* signature);

    struct bls_signature* bls_sign_message(const struct bls_config* cfg, const void* msg, size_t msg_len, const char* sig_share, size_t share_len);
    bool bls_verify_signatures(const struct bls_config* cfg, const void* msg,  size_t msg_len, const struct ovs_list* sig_shares);
    bool bls_verify_signature(const struct bls_config* cfg, const void* msg, size_t msg_len, const void* msg_signature, size_t sig_len);
    bool bls_verify_signatures_existing(const struct bls_config* cfg, const void* msg_signature, const struct ovs_list* sig_shares);
#ifdef __cplusplus
};
#endif

#endif // BLS_UTILS_H
