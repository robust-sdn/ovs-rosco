#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <inttypes.h>
#include <time.h>

#include "bls_utils.h"

long long int current_time_nsec(void)
{
    struct timespec cur_time;
    clock_gettime(CLOCK_REALTIME, &cur_time);
    return cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
}

int main(int argc, char* argv[])
{
    char system_param[PATH_MAX+1];
    char pairing_param[PATH_MAX+1];
    char public_key_file[PATH_MAX+1];

    if(argc < 4) {
        fprintf(stderr, "USAGE: testbls cfg_path signature_file message\n");
        exit(1);
    }

    const char* cfg_path = argv[1];
    const char* sig_path = argv[2];
    const char* message = argv[3];

    size_t message_len = strlen(message);
    printf("message size: %d\n", (int)message_len);
    
    char sig_bytes[1024];
    int sig_file = open(sig_path, O_RDONLY);
    if(sig_file == -1) {
        printf("unable to open file\n");
        exit(1);
    }
    size_t sig_len = read(sig_file, sig_bytes, sizeof(sig_bytes)) - 1;
    close(sig_file);
    printf("Signature size: %d\n", (int)sig_len);
    
    
    printf("Initializing BLS...\n");
    //struct bls_config* cfg = bls_config_init("/usr/local/share/pbc/system.param", "/usr/local/share/pbc/pairing.param", "/usr/local/share/pbc/bls_pk_file");
    snprintf(system_param, PATH_MAX, "%s/%s", cfg_path, "system.param");
    snprintf(pairing_param, PATH_MAX, "%s/%s", cfg_path, "pairing.param");
    snprintf(public_key_file, PATH_MAX, "%s/%s", cfg_path, "bls_pk_file");

    struct bls_config* cfg = bls_config_init(system_param, pairing_param, public_key_file);
    if(cfg == NULL) {
        fprintf(stderr, "Could not initialize BLS\n");
        exit(1);
    }

    printf("Verifying signature...\n");
    uint64_t stime = current_time_nsec();
    if(bls_verify_signature(cfg, message, message_len, (char*)sig_bytes, sig_len)) {
        printf("CORRECT!\n");
    } else {
        printf("INCORRECT\n");
    }
    uint64_t etime = current_time_nsec();
    printf("TIME: %" PRIu64 "\n", etime - stime);

    printf("Cleaning up BLS config...\n");
    bls_config_destroy(cfg);
    return 0;
}
