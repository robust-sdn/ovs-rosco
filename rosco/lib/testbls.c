#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <inttypes.h>
#include <time.h>

#include "bls_utils.h"

long long int current_time_nsec(void)
{
    struct timespec cur_time;
    clock_gettime(CLOCK_REALTIME, &cur_time);
    return cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
}

int main(int argc, char* argv[])
{
    char share_path[PATH_MAX+1];
    char system_param[PATH_MAX+1];
    char pairing_param[PATH_MAX+1];
    char public_key_file[PATH_MAX+1];

    if(argc < 3) {
        fprintf(stderr, "USAGE: testbls cfg_dir message\n");
        exit(1);
    }

    const char* cfg_path = argv[1];
    const char* message = argv[2];
    int stop_at_count = -1;
    if(argc >= 4) {
        stop_at_count = atoi(argv[3]);
        //printf("Stopping after reading %d shares\n", stop_at_count);
    }

	int repetitions = 1;
	if(argc >= 5) {
		repetitions = atoi(argv[4]);
		//printf("Verifying %d times\n", repetitions);
	}

    char message_bytes[1024];
    int msg_file = open(message, O_RDONLY);
    if(msg_file == -1) {
        printf("unable to open file\n");
        exit(1);
    }
    size_t message_len = read(msg_file, message_bytes, sizeof(message_bytes)) - 1;
    close(msg_file);
    message = message_bytes;
    //printf("message size: %d\n", (int)message_len);
    
    // size_t message_len = strlen(message);
    snprintf(share_path, PATH_MAX, "%s/%s", cfg_path, "shares");

    FILE* share_file = fopen(share_path, "r");
    if (share_file == NULL) {
        perror("Error opening share_file");
        exit(1);
    }

    //printf("Initializing BLS...\n");
    //struct bls_config* cfg = bls_config_init("/usr/local/share/pbc/system.param", "/usr/local/share/pbc/pairing.param", "/usr/local/share/pbc/bls_pk_file");
    snprintf(system_param, PATH_MAX, "%s/%s", cfg_path, "system.param");
    snprintf(pairing_param, PATH_MAX, "%s/%s", cfg_path, "pairing.param");
    snprintf(public_key_file, PATH_MAX, "%s/%s", cfg_path, "bls_pk_file");

    struct bls_config* cfg = bls_config_init(system_param, pairing_param, public_key_file);
    if(cfg == NULL) {
        fprintf(stderr, "Could not initialize BLS\n");
        exit(1);
    }

    struct ovs_list signature_shares;
    ovs_list_init(&signature_shares);
    char share[64];
    size_t share_len;
    int node_id;

    //printf("Signing message...\n");
    int i = 1;
    while(fscanf(share_file, "%d %64s", &node_id, share) > 0) {
        if(stop_at_count > 0 && i > stop_at_count) {
            break;
        }
        share_len = strlen(share);
        //printf("Creating signature for node: %d share: %s\n", node_id, share);
        struct bls_signature* sig = bls_sign_message(cfg, message, message_len, share, share_len);
        if(sig == NULL) {
            fprintf(stderr, "Unable to sign message\n");
            bls_config_destroy(cfg);
            exit(1);
        }
        sig->node_id = node_id;
        ovs_list_push_back(&signature_shares, &sig->node);
        i++;
    }
    fclose(share_file);

    //printf("Verifying signature shares...\n");
	for(int j = 0; j < 50; j++) {
		bool verify = false;
		void* signature = bls_compute_signature(cfg, message, message_len);
		long long stime = current_time_nsec();
		for(int i = 0; i < repetitions; i++) {
			verify = bls_verify_signatures_existing(cfg, signature, &signature_shares);
			//verify = bls_verify_signatures(cfg, message, message_len, &signature_shares);
		}
	   	long long etime = current_time_nsec();
		bls_signature_destroy(signature);
		/*
		if(verify) {
			printf("CORRECT!\n");
		} else {
			printf("INCORRECT\n");
		}
		*/
		//printf("TIME: %" PRIu64 "\n", etime - stime);
		printf("%" PRIu64 "\n", etime - stime);
	}

    //printf("Cleaning up signature list...\n");
    struct bls_signature *sig_node, *next_node;
    LIST_FOR_EACH_SAFE (sig_node, next_node, node, &signature_shares) {
        free(sig_node->signature);
        free(sig_node);
    }

    //printf("Cleaning up BLS config...\n");
    bls_config_destroy(cfg);
    return 0;
}
