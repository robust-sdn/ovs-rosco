#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <vector>
#include <string>

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

#include <openvswitch/list.h>

#include "PBC/PBC.h"
#include "bls_utils.h"
#include "lagrange.h"
#include "systemparam.h"

//#define DEBUG 1
#define DEBUG_OUT stdout

struct bls_config {
    SystemParam* sysParam;
    G1* publicKey;
};

#define NSEC_PER_SEC 1000000000
static inline uint64_t get_time_ns()
{
    struct timespec ts;
    if(clock_gettime(CLOCK_REALTIME, &ts) == -1) {
        fprintf(DEBUG_OUT, "Unable to retrieve current time: %s\n", strerror(errno));
        exit(1);
    }
    return ts.tv_sec * NSEC_PER_SEC + ts.tv_nsec;
}
#undef NSEC_PER_SEC

static inline
ssize_t bls_read_param_file(const char* param_path, char* buf, size_t buf_size)
{
    int fd = open(param_path, O_RDONLY);
    if(fd == -1) {
        return -1;
    }
    ssize_t bytes_read = read(fd, buf, buf_size);
    if(bytes_read == -1) {
        return -1;
    }
    close(fd);
    return bytes_read;
}

int bls_config_getT(const struct bls_config* cfg)
{
    if(cfg && cfg->sysParam) {
        return (int)(cfg->sysParam->get_t());
    }
    return -1;
}

struct bls_config* bls_config_init(const char* sys_param_path, const char* pairing_param_path, const char* public_key_path)
{
    struct bls_config* cfg;
    char public_key_buf[512];
    ssize_t public_key_length;

    cfg = (struct bls_config*)malloc(sizeof(struct bls_config));
    memset(cfg, 0x00, sizeof(struct bls_config));

    try {
        cfg->sysParam = new SystemParam(pairing_param_path, sys_param_path);
        if (cfg->sysParam->get_Pairing().isPairingPresent() == false) {
            delete (cfg->sysParam);
            free(cfg);
            fprintf(DEBUG_OUT, "Failed to initialize SystemParam\n");
            return NULL;
        }
        public_key_length = bls_read_param_file(public_key_path, public_key_buf, sizeof(public_key_buf));
        if (public_key_length <= 0) {
            delete (cfg->sysParam);
            free(cfg);
            fprintf(DEBUG_OUT, "Read from PK file failed: %d\n", errno);
            return NULL;
        }
        cfg->publicKey = new G1((cfg->sysParam->get_Pairing()), (unsigned char*)public_key_buf, public_key_length, false, 10);
    } catch(runtime_error e) {
        fprintf(DEBUG_OUT, "Runtim Error recevied in bls_config_init: %s\n", e.what());
        if(cfg->publicKey != NULL) {
            delete cfg->publicKey;
        }
        if(cfg->sysParam != NULL) {
            delete cfg->sysParam;
        }
        free(cfg);
        return NULL;
    }
    return cfg;
}

void bls_config_destroy(struct bls_config* cfg)
{
    delete cfg->publicKey;
    delete cfg->sysParam;
    free(cfg);
}

struct bls_signature*
bls_sign_message(const struct bls_config* cfg, const void* msg, size_t msg_len, const char* sig_share, size_t share_len)
{
    struct bls_signature* retval = NULL;
    string signatureString;

    try {
        const Pairing& e = cfg->sysParam->get_Pairing();
        Zr share(e, (unsigned char*)sig_share, share_len, 10);
#ifdef DEBUG
        share.dump(DEBUG_OUT, (char*)"Share: ", 10);
#endif

        unsigned char hash_buf[20];
        gcry_md_hash_buffer(GCRY_MD_SHA1, hash_buf, msg, msg_len);
        G1 msgHashG1(e, (void*)hash_buf, sizeof(hash_buf));
#ifdef DEBUG
        msgHashG1.dump(DEBUG_OUT, (char*)"Message Hash: ", 10);
#endif

        G1 signatureShare = msgHashG1^share;
#ifdef DEBUG
        signatureShare.dump(DEBUG_OUT, (char*)"Signature share: ", 10);
#endif
        signatureString = signatureShare.toString(false);
    } catch(runtime_error e) {
        fprintf(DEBUG_OUT, "Runtime Error recevied in bls_sign_message: %s\n", e.what());
        return NULL;
    }

    if (signatureString.size() <= 0) {
        fprintf(DEBUG_OUT, "Signature string size <= 0\n");
        return NULL;
    }

    retval = (struct bls_signature*)malloc(sizeof(struct bls_signature));
    retval->signature_length = signatureString.size();
    retval->signature = malloc(retval->signature_length);
    memcpy(retval->signature, signatureString.data(), retval->signature_length);

    return retval;
}

void* bls_compute_signature(const struct bls_config* cfg, const void* msg, size_t msg_len)
{
#ifdef DEBUG
    uint64_t start_time = get_time_ns();
#endif
    
    const Pairing& e = cfg->sysParam->get_Pairing();
    
    unsigned char hash_buf[20];
    gcry_md_hash_buffer(GCRY_MD_SHA1, hash_buf, msg, msg_len);
    G1 msgHashG1(e, (void*)hash_buf, sizeof(hash_buf));
#ifdef DEBUG
    msgHashG1.dump(DEBUG_OUT, (char*)"Message Hash: ", 10);
#endif

    GT* signature = new GT(e(*(cfg->publicKey), msgHashG1));
#ifdef DEBUG
    signature->dump(DEBUG_OUT, (char*)"Message Signature: ", 10); 
#endif

#ifdef DEBUG
    uint64_t end_time = get_time_ns();
    fprintf(DEBUG_OUT, "Quorum Signature Time: %" PRIu64 "\n", end_time - start_time);
#endif

    return (void*)signature;
}

void bls_signature_destroy(void* signature)
{
    GT* bls_signature = (GT*)signature;
    delete bls_signature;
}

bool 
bls_verify_signatures_existing(const struct bls_config* cfg, const void* msg_signature, const struct ovs_list* sig_shares)
{
#ifdef DEBUG
    uint64_t start_time = get_time_ns();
#endif

    struct bls_signature* sig_node;
    const Pairing& e = cfg->sysParam->get_Pairing();
    vector <Zr> indices; vector <G1> shares;

    GT* pairing_signature = (GT*)msg_signature;

    LIST_FOR_EACH(sig_node, node, sig_shares) {
#ifdef DEBUG
        char sigFileName[32];
        snprintf(sigFileName, sizeof(sigFileName), "/tmp/%d_sig.out", sig_node->node_id);
        int sigFd = open(sigFileName, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
        if(sigFd == -1) {
            fprintf(DEBUG_OUT, "Unable to open %s\n", sigFileName);
        } else {
            if(write(sigFd, sig_node->signature, sig_node->signature_length) == -1) {
                fprintf(DEBUG_OUT, "Failed to write signature to %s\n", sigFileName);
            }
            close(sigFd);
        }
#endif
        indices.push_back(Zr(e, (signed long)(sig_node->node_id)));
        shares.push_back(G1(e, (unsigned char*)sig_node->signature, sig_node->signature_length, false));
#ifdef DEBUG
        fprintf(DEBUG_OUT, "NODE ID: %d\n", sig_node->node_id);
        shares.back().dump(DEBUG_OUT, (char*)"Signature share: ", 10);
#endif
    }                        
    //pushing evaluation at zero
    Zr alpha(e,(long)0);
    vector<Zr> coeffs = lagrange_coeffs(indices, alpha);
    G1 tempSignature = lagrange_apply(coeffs, shares);
    bool verified = e(cfg->sysParam->get_U(), tempSignature) == (*pairing_signature);

#ifdef DEBUG
    uint64_t end_time = get_time_ns();
    fprintf(DEBUG_OUT, "Verify Time: %" PRIu64 "\n", end_time - start_time);
#endif

    return verified;
}

bool
bls_verify_signature(const struct bls_config* cfg, const void* msg, size_t msg_len, const void* msg_signature, size_t sig_len)
{
#ifdef DEBUG
    uint64_t start_time = get_time_ns();
#endif
    const Pairing& e = cfg->sysParam->get_Pairing();
    GT* msg_public_sig = (GT*)bls_compute_signature(cfg, msg, msg_len);
    GT msg_sig = GT(e, (unsigned char*)msg_signature, sig_len, 16);
    bool verified = msg_sig == (*msg_public_sig);
#ifdef DEBUG
    uint64_t end_time = get_time_ns();
    fprintf(DEBUG_OUT, "Verify Time: %" PRIu64 "\n", end_time - start_time);
#endif
    bls_signature_destroy(msg_public_sig);
    return verified;
}

bool 
bls_verify_signatures(const struct bls_config* cfg, const void* msg, size_t msg_len, const struct ovs_list* sig_shares)
{
    void* signature = bls_compute_signature(cfg, msg, msg_len);
    bool ret = bls_verify_signatures_existing(cfg, signature, sig_shares);
    bls_signature_destroy(signature);
    return ret;
}
